#!/bin/bash
echo 'Enable and start mariadb'
sudo systemctl enable mariadb
sudo systemctl start mariadb
echo 'Creating the database'
sudo mariadb < /vagrant/pre_wordpress.sql

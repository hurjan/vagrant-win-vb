

# vagrant-win-vb - libvirt

testrepo for experimenting with Vagrant cross providers and OS, in
particular libvirt on Linux to virtual box on windows/mac

-   libvirt is using <code>rsync</code> for synced folder, this breaks
    on windows


## TESTA MED EN RUBRIK

    CREATE DATABASE wpdb;
    CREATE USER "wpuser"@"localhost" IDENTIFIED BY "wppass";
    GRANT ALL PRIVILEGES ON wpdb.* TO "wpuser"@"localhost";
    FLUSH PRIVILEGES;	


### TESTA EN ANNAN RUBRIK

    
    MariaDB [(none)]> CREATE DATABASE wpdb;
    
    MariaDB [(none)]> CREATE USER "wpuser"@"localhost" IDENTIFIED BY "wppass";
    
    MariaDB [(none)]> GRANT ALL PRIVILEGES ON wpdb.* TO "wpuser"@"localhost";
    
    MariaDB [(none)]> FLUSH PRIVILEGES;

-   TESTAR EN SÅN HÄR BULLETPOINT
    
        MariaDB [(none)]> CREATE DATABASE wpdb;
        Query OK, 1 row affected (0.00 sec)
        MariaDB [(none)]> CREATE USER "wpuser"@"localhost" IDENTIFIED BY "wppass";
        MariaDB [(none)]> GRANT ALL PRIVILEGES ON wpdb.* TO "wpuser"@"localhost";
        Query OK, 1 row affected (0.00 sec)
        
        MariaDB [(none)]> FLUSH PRIVILEGES;
        Query OK, 1 row affected (0.00 sec)


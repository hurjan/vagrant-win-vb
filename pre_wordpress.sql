-- Provision script for db pre wordpress install 	  
  CREATE DATABASE wpdb;
  CREATE USER "wpuser"@"localhost" IDENTIFIED BY "wppass";
  GRANT ALL PRIVILEGES ON wpdb.* TO "wpuser"@"localhost";
  FLUSH PRIVILEGES;

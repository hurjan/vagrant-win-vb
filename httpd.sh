#!/bin/bash
echo 'copying httpd.conf' 
sudo cp /vagrant/httpd.conf /etc/httpd/conf.d/
echo 'enable and start httpd'
sudo systemctl enable httpd
sudo systemctl start httpd

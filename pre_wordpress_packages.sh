sudo dnf update -y
sudo dnf install -y php php-mysqlnd mariadb-server httpd

echo 'enable and start mariadb'
sudo systemctl enable mariadb
sudo systemctl start mariadb
echo 'Creating the database'
sudo mariadb < /vagrant/pre_wordpress.sql


echo 'downloading wordpress'
wget https://wordpress.org/latest.tar.gz
echo 'unpacking into html/'
sudo tar -xzvf latest.tar.gz -C /var/www/html/
echo 'copying wp-config'
sudo cp /vagrant/wp-config.php /var/www/html/wordpress/ 


echo 'copying httpd.conf' 
sudo cp /vagrant/httpd.conf /etc/httpd/conf.d/
echo 'enable and start httpd'
sudo systemctl enable httpd
sudo systemctl start httpd


